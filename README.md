# Rest Api User and Task Management

## Requirement
- Postgresql (database)
- Go ver ^1.21

## How To Run

### Creating Database
- first create database on postgresql
- update env with dbname, user, password based on your system

### Run App
- install package
```bash
go mod vendor
```
- run app 
```bash
go run .
```
- open collection on yout api client *you can use collection on collection folder*


## Information App

### List EndPoint 
	- [POST]     "/login"       -> user login 
	- [GET]     "/users"        -> get all user [need auth]
	- [GET]     "/users/:id"    -> get user by id [need auth]
	- [PUT]     "/users/:id"    -> update user [need auth]
	- [DELETE]  "/users/:id"    -> delete user [need auth]
	- [POST]    "/users"        -> create user 
    - [GET]     "/tasks"        -> get all task [need auth]
	- [GET]     "/tasks/:id"    -> get task by id [need auth]
	- [POST]    "/tasks"        -> create task [need auth]
	- [PUT]     "/tasks/:id"    -> update task [need auth]
	- [DELETE]  "/tasks/:id"    -> delete task [need auth]

## Contributors
    [Gitlab MAbduhI](https://gitlab.com/MAbduhI)
    [Github MAbduhI](https://github.com/MAbduhI)