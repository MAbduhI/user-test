// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package main

import (
	"context"
	"gin-user-api/app"
	"gin-user-api/config"
)

func main() {
	// log.Println("test")
	// r := gin.Default()
	// r.GET("/test", func(c *gin.Context) {
	// 	c.JSON(200, gin.H{
	// 		"test": "success",
	// 	})
	// })
	// r.Run()
	var ctx context.Context
	conf := config.Init()
	server := app.NewApp(conf)
	server.Start(ctx)
	server.Run(ctx)

}
