// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package module

import (
	"golang.org/x/crypto/bcrypt"
)

var CreatePassword = func(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

var CheckPassword = func(password string, hashedPassword string) (bool, error) {
	var ok bool
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		return ok, err
	}
	return true, nil
}
