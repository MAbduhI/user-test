package module

import (
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
)

//const TOKEN_EXPIRATION_TIME = 24 * time.Hour

const SECRET = "dDBrM25jMGRl"

type TokenPayload struct {
	jwt.StandardClaims
	Email string `json:"email"`
}

type TokenMetadata struct {
	Email string
}

var GenerateToken = func(email string, exp time.Duration) (string, error) {
	payload := TokenPayload{
		jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(exp).Unix(),
			Issuer:    "sewadingin",
		},
		email,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	return token.SignedString([]byte(SECRET))
}

var ExtractTokenMetadata = func(token string) (TokenMetadata, error) {
	var metadata TokenMetadata
	var err error
	parsedToken, err := jwt.ParseWithClaims(token, &TokenPayload{}, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, errors.New("unexpected signing method in auth token")
		}
		return []byte(SECRET), nil
	})
	if err != nil {
		return metadata, err
	}

	payload := parsedToken.Claims.(*TokenPayload)
	metadata.Email = payload.Email
	return metadata, err
}

var VerifyToken = func(strToken string) (*jwt.Token, error) {
	return jwt.Parse(strToken, func(token *jwt.Token) (i interface{}, err error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(SECRET), nil
	})
}
