// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package request

type TaskRequest struct {
	UsersID     int    `json:"user_id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      string `json:"status"`
}
