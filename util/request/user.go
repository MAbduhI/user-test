// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package request

type (
	UserRegis struct {
		Name     string `json:"name"`
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	UserLogin struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
)
