// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package response

type (
	TasksResponse struct {
		ID          int    `json:"id"`
		UserID      int    `json:"user_id"`
		Title       string `json:"title"`
		Description string `json:"description"`
		Status      string `json:"status"`
		UserName    string `json:"user_name,omitempty"`
	}
)
