// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package response

import "net/http"

type ResponseData struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    any    `json:"data"`
}

func ResponseTemp(code int, message string, data any) ResponseData {
	return ResponseData{
		Code:    code,
		Message: message,
		Data:    data,
	}
}

func ResponseSuccess(data any) (ResponseData, int) {
	return ResponseTemp(http.StatusOK, "success", data), http.StatusOK
}
