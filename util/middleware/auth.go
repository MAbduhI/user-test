package middleware

import (
	"gin-user-api/module"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.Request.Header.Get("Authorization")
		parsed := strings.Split(authHeader, " ")
		if len(parsed) < 2 {
			c.JSON(http.StatusUnauthorized, gin.H{
				"message": "not authorized",
			})
			c.Abort()

			return
		}

		token := parsed[1]
		metadata, err := module.ExtractTokenMetadata(token)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"message": "invalid token",
			})
			c.Abort()
			return
		}
		c.Set("metadataFromToken", metadata)
		c.Next()

	}
}
