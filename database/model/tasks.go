// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package model

import (
	"time"

	gorm "gorm.io/gorm"
)

type (
	Tasks struct {
		ID          int       `gorm:"primarykey;column:id"`
		UsersID     int       `gorm:"column:user_id"`
		Title       string    `gorm:"column:title"`
		Description string    `gorm:"column:description"`
		Status      string    `gorm:"column:status;default:'pending'"`
		CreatedAt   time.Time `gorm:"column:created_at"`
		UpdatedAt   time.Time `gorm:"column:updated_at"`
		User        Users     `gorm:"foreignKey:UsersID"`
	}
)

func (Tasks) TableName() string {
	return "tasks"
}
func (m Tasks) Create(dbt *gorm.DB) error {
	return dbt.Create(&m).Error
}
func (m Tasks) Update(dbt *gorm.DB) error {
	m.UpdatedAt = time.Now()
	return dbt.Select("*").Updates(&m).Error
}
func (m Tasks) Delete(dbt *gorm.DB) error {
	return dbt.Delete(&Tasks{}, m.ID).Error
}
func (m Tasks) QByModel(dbt *gorm.DB) error {
	return dbt.First(&m).Error
}
