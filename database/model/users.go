// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package model

import (
	"time"

	gorm "gorm.io/gorm"
)

type (
	Users struct {
		ID        int       `gorm:"primarykey;column:id" json:"id"`
		Name      string    `gorm:"column:name" json:"name"`
		Email     string    `gorm:"column:email" json:"email"`
		Password  string    `gorm:"column:password" json:"password"`
		CreatedAt time.Time `gorm:"column:created_at" json:"created_at"`
		UpdatedAt time.Time `gorm:"column:updated_at" json:"updated_at"`
	}
)

func (Users) TableName() string {
	return "users"
}
func (m Users) Create(dbt *gorm.DB) error {
	return dbt.Create(&m).Error
}
func (m Users) Update(dbt *gorm.DB) error {
	m.UpdatedAt = time.Now()
	return dbt.Select("*").Updates(&m).Error
}
func (m Users) Delete(dbt *gorm.DB) error {
	return dbt.Delete(&Users{}, m.ID).Error
}
func (m Users) QByModel(dbt *gorm.DB) error {
	return dbt.First(&m).Error
}
