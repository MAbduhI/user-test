// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package ruser

import (
	"context"
	"gin-user-api/database/model"

	"gorm.io/gorm"
)

func (r repository) GetAllUser(ctx context.Context) ([]model.Users, error) {
	var users []model.Users
	db := r.provider.Db(ctx).(*gorm.DB)
	err := db.Model(model.Users{}).Select("*").Scan(&users).Error
	return users, err
}
func (r repository) GetUserByEmail(ctx context.Context, email string) (model.Users, error) {
	var users model.Users
	db := r.provider.Db(ctx).(*gorm.DB)
	err := db.Model(model.Users{}).First(&users, "email = ?", email).Error
	return users, err
}
func (r repository) GetUserByID(ctx context.Context, id uint) (model.Users, error) {
	var users model.Users
	db := r.provider.Db(ctx).(*gorm.DB)
	err := db.Model(model.Users{}).First(&users, "id = ?", id).Error
	return users, err
}
