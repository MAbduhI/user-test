// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package ruser

import (
	"context"
	"gin-user-api/database/model"
	"gin-user-api/database/provider"

	"gorm.io/gorm"
)

type repository struct {
	provider provider.DatabaseI
}

func NewRepository(provider provider.DatabaseI) RepositoryI {
	return repository{
		provider,
	}
}

type RepositoryI interface {
	DoTransaction(ctx context.Context, fn func(ctxWithTx context.Context, dbt *gorm.DB) error) error

	GetAllUser(ctx context.Context) ([]model.Users, error)
	GetUserByEmail(ctx context.Context, email string) (model.Users, error)
	GetUserByID(ctx context.Context, id uint) (model.Users, error)
}

func (r repository) DoTransaction(ctx context.Context, fn func(ctxWithTx context.Context, dbt *gorm.DB) error) error {
	return r.provider.WithRollback(ctx, fn)
}
