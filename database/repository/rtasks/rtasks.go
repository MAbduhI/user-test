// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package rtasks

import (
	"context"
	"gin-user-api/database/model"
	"gin-user-api/database/provider"

	"gorm.io/gorm"
)

type repository struct {
	provider provider.DatabaseI
}

func NewRepository(provider provider.DatabaseI) RepositoryI {
	return repository{
		provider,
	}
}

type RepositoryI interface {
	DoTransaction(ctx context.Context, fn func(ctxWithTx context.Context, dbt *gorm.DB) error) error
	GetAllTask(ctx context.Context) ([]model.Tasks, error)
	GetTaskByID(ctx context.Context, id int) (model.Tasks, error)
}

func (r repository) DoTransaction(ctx context.Context, fn func(ctxWithTx context.Context, dbt *gorm.DB) error) error {
	return r.provider.WithRollback(ctx, fn)
}
