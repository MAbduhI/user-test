// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package rtasks

import (
	"context"
	"gin-user-api/database/model"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func (r repository) GetAllTask(ctx context.Context) ([]model.Tasks, error) {
	var tasks []model.Tasks
	db := r.provider.Db(ctx).(*gorm.DB)
	err := db.Model(model.Tasks{}).Preload(clause.Associations).Find(&tasks).Error
	return tasks, err
}

func (r repository) GetTaskByID(ctx context.Context, id int) (model.Tasks, error) {
	var tasks model.Tasks
	db := r.provider.Db(ctx).(*gorm.DB)
	err := db.Model(model.Tasks{}).Preload(clause.Associations).First(&tasks, id).Error
	return tasks, err
}
