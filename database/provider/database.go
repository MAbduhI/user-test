// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package provider

import (
	"context"
	"gin-user-api/database/model"
	"log"
	"os"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	ormlog "gorm.io/gorm/logger"
)

type postgresDb struct {
	db *gorm.DB
}

type DatabaseI interface {
	Db(ctx context.Context) interface{}
	WithRollback(ctx context.Context, fn func(ctxWithTx context.Context, dbt *gorm.DB) error) error
}

func NewPostgres(dsn string) (DatabaseI, error) {
	newLogger := ormlog.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer

		ormlog.Config{
			SlowThreshold:             5 * time.Second, // Slow SQL threshold
			Colorful:                  true,            // Disable color
			IgnoreRecordNotFoundError: true,            // Ignore ErrRecordNotFound error for logger
			LogLevel:                  ormlog.Error,    // Log level
		},
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
		Logger:                                   newLogger,
	})
	if err != nil {
		return postgresDb{db: db}, err
	}

	db.AutoMigrate(
		&model.Users{}, &model.Tasks{},
	)

	log.Println("[INFO] success connected to database")

	return postgresDb{db: db}, nil
}

func (d postgresDb) Db(ctx context.Context) interface{} {
	tx := ctx.Value("txContext")
	if tx == nil {
		return d.db
	}
	return tx.(*gorm.DB)
}

func (d postgresDb) WithRollback(ctx context.Context, fn func(ctxWithTx context.Context, dbt *gorm.DB) error) error {
	tx := d.db.Begin()
	ctxWithTx := context.WithValue(ctx, tx, "txContext")
	errFn := fn(ctxWithTx, tx)
	if errFn != nil {
		errRlbck := tx.Rollback().Error
		if errRlbck != nil {
			log.Println("[ERROR] go error when rollback :" + errRlbck.Error())
			//return errRlbck
		}
		return errFn
	}

	errCmmt := tx.Commit().Error
	if errCmmt != nil {
		log.Println("[ERROR] go error when commit :" + errCmmt.Error())
	}
	return errFn
}
