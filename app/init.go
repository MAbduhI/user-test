// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package app

import (
	"context"
	"fmt"
	"gin-user-api/app/handler/htasks"
	"gin-user-api/app/handler/husers"
	"gin-user-api/app/usecase/utasks"
	"gin-user-api/app/usecase/uusers"
	"gin-user-api/config"
	"gin-user-api/database/provider"
	"gin-user-api/database/repository/rtasks"
	"gin-user-api/database/repository/ruser"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

type appStruct struct {
	config *config.Config

	ginNew    *gin.Engine
	errServer chan error
}
type Application interface {
	Start(ctx context.Context) error
	Run(ctx context.Context)
}

func NewApp(config *config.Config) Application {
	return &appStruct{
		config: config,
	}
}
func (a *appStruct) Start(ctx context.Context) error {
	// Repository Init
	var err error
	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("DBHOST"), os.Getenv("DBPORT"), os.Getenv("DBUSER"), os.Getenv("DBPASS"), os.Getenv("DBNAME"))
	db, err := provider.NewPostgres(dsn)
	if err != nil {
		log.Fatalln("[ERROR] got error when try to connected database")
	}

	repoUser := ruser.NewRepository(db)
	repoTask := rtasks.NewRepository(db)

	ucaseUser := uusers.NewUsecase(repoUser)
	ucaseTask := utasks.NewUsecase(repoTask, repoUser)

	a.ginNew = gin.New()
	a.ginNew.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		// your custom format
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))

	a.ginNew.Use(gin.Recovery())
	a.ginNew.Use(JSONMiddleware())
	husers.NewHandler(a.ginNew, ucaseUser)
	htasks.NewHandler(a.ginNew, ucaseTask)

	return nil
}
func JSONMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Next()
	}
}
