// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package utasks

import (
	"context"
	"errors"
	"gin-user-api/database/model"
	"gin-user-api/database/repository/rtasks"
	"gin-user-api/database/repository/ruser"
	"gin-user-api/util/request"
	"gin-user-api/util/response"

	"gorm.io/gorm"
)

type usecase struct {
	repoTask rtasks.RepositoryI
	repoUser ruser.RepositoryI
}

type Usecase interface {
	GetAllTask(ctx context.Context) ([]response.TasksResponse, error)
	GetTask(ctx context.Context, id int) (response.TasksResponse, error)
	CreateTasks(ctx context.Context, body request.TaskRequest) error
	UpdateTasks(ctx context.Context, body request.TaskRequest, task_id int) error
	DeleteTasks(ctx context.Context, task_id int) error
}

func NewUsecase(
	repoTask rtasks.RepositoryI,
	repoUser ruser.RepositoryI,
) Usecase {
	return &usecase{
		repoTask,
		repoUser,
	}
}

func (u usecase) GetAllTask(ctx context.Context) ([]response.TasksResponse, error) {
	var resp []response.TasksResponse
	tasks, err := u.repoTask.GetAllTask(ctx)
	if err != nil {
		errNew := errors.New("error on finding task")
		return resp, errNew
	}
	for _, d := range tasks {
		resp = append(resp, response.TasksResponse{
			ID:          d.ID,
			UserID:      d.UsersID,
			Title:       d.Title,
			Description: d.Description,
			Status:      d.Status,
			UserName:    d.User.Name,
		})
	}

	return resp, nil
}
func (u usecase) GetTask(ctx context.Context, id int) (response.TasksResponse, error) {
	var resp response.TasksResponse
	tasks, err := u.repoTask.GetTaskByID(ctx, id)
	if err != nil {
		errNew := errors.New("error on finding task")
		return resp, errNew
	}

	return response.TasksResponse{
		ID:          tasks.ID,
		UserID:      tasks.UsersID,
		Title:       tasks.Title,
		Description: tasks.Description,
		Status:      tasks.Status,
		UserName:    tasks.User.Name,
	}, nil
}

func (u usecase) CreateTasks(ctx context.Context, body request.TaskRequest) error {
	// Check user exist
	_, err := u.repoUser.GetUserByID(ctx, uint(body.UsersID))
	if errors.Is(err, gorm.ErrRecordNotFound) || err != nil {
		errNew := errors.New("got error user not found")
		return errNew
	}

	task := model.Tasks{
		UsersID:     body.UsersID,
		Title:       body.Title,
		Description: body.Description,
		Status:      body.Status,
	}

	errTx := u.repoTask.DoTransaction(ctx, func(ctxWithTx context.Context, dbt *gorm.DB) error {
		err := task.Create(dbt)
		if err != nil {
			errNew := errors.New("got error when create tasks")
			return errNew
		}
		return nil
	})
	if errTx != nil {
		errNew := errors.New("error when create task : " + errTx.Error())
		return errNew
	}
	return nil
}

func (u usecase) UpdateTasks(ctx context.Context, body request.TaskRequest, task_id int) error {
	// Check user exist
	task_last, err := u.repoTask.GetTaskByID(ctx, task_id)
	if errors.Is(err, gorm.ErrRecordNotFound) || err != nil {
		errNew := errors.New("got error user not found")
		return errNew
	}
	if task_last.UsersID != body.UsersID || body.UsersID != 0 {
		task_last.UsersID = body.UsersID
	}
	if task_last.Title != body.Title || body.Title != "" {
		task_last.Title = body.Title
	}
	if task_last.Description != body.Description || body.Description != "" {
		task_last.Description = body.Description
	}
	if task_last.Status != body.Status || body.Status != "" {
		task_last.Status = body.Status
	}
	errTx := u.repoTask.DoTransaction(ctx, func(ctxWithTx context.Context, dbt *gorm.DB) error {
		err := task_last.Update(dbt)
		if err != nil {
			errNew := errors.New("got error when update tasks")
			return errNew
		}
		return nil
	})
	if errTx != nil {
		errNew := errors.New("error when update task : " + errTx.Error())
		return errNew
	}
	return nil
}

func (u usecase) DeleteTasks(ctx context.Context, task_id int) error {
	// Check user exist
	task_last, err := u.repoTask.GetTaskByID(ctx, task_id)
	if errors.Is(err, gorm.ErrRecordNotFound) || err != nil {
		errNew := errors.New("got error user not found")
		return errNew
	}
	errTx := u.repoTask.DoTransaction(ctx, func(ctxWithTx context.Context, dbt *gorm.DB) error {
		err := task_last.Delete(dbt)
		if err != nil {
			errNew := errors.New("got error when delete tasks")
			return errNew
		}
		return nil
	})
	if errTx != nil {
		errNew := errors.New("error when delete task : " + errTx.Error())
		return errNew
	}
	return nil
}
