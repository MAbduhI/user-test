// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package uusers

import (
	"context"
	"gin-user-api/database/repository/ruser"
	"gin-user-api/util/request"
	"gin-user-api/util/response"
)

type usecase struct {
	repoUsers ruser.RepositoryI
}

type Usecase interface {
	GetAllUser(ctx context.Context) ([]response.UsersResp, error)
	GetUserById(ctx context.Context, id uint) (*response.UsersResp, error)
	CreateUser(ctx context.Context, body request.UserRegis) error
	UpdateUser(ctx context.Context, body request.UserRegis, user_id uint) error
	DeleteUser(ctx context.Context, user_id uint) error
	Login(ctx context.Context, body request.UserLogin) (string, error)
}

func NewUsecase(
	repoUsers ruser.RepositoryI,
) Usecase {
	return &usecase{
		repoUsers,
	}
}
