package uusers

import (
	"context"
	"errors"
	"gin-user-api/module"
	"gin-user-api/util/request"
	"time"
)

func (u usecase) Login(ctx context.Context, body request.UserLogin) (string, error) {
	users, err := u.repoUsers.GetUserByEmail(ctx, body.Email)
	if err != nil {
		errNew := errors.New("users not found")
		return "", errNew
	}
	ok, err := module.CheckPassword(body.Password, users.Password)
	if err != nil && !ok {
		errNew := errors.New("password missmatch")
		return "", errNew
	}
	token, err := module.GenerateToken(body.Email, (24*7)*time.Hour)
	if err != nil {
		errNew := errors.New("error when create token")
		return "", errNew
	}
	return token, nil
}
