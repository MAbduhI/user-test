// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package uusers

import (
	"context"
	"errors"
	"fmt"
	"gin-user-api/database/model"
	"gin-user-api/module"
	"gin-user-api/util/request"

	"gorm.io/gorm"
)

func (u usecase) CreateUser(ctx context.Context, body request.UserRegis) error {
	// find user by email
	_, err := u.repoUsers.GetUserByEmail(ctx, body.Email)
	if !errors.Is(err, gorm.ErrRecordNotFound) || err == nil {
		errNew := errors.New("user email already exist")
		return errNew
	}
	password, err := module.CreatePassword(body.Password)
	if err != nil {
		errNew := errors.New("got error when create user")
		return errNew
	}
	user := model.Users{
		Name:     body.Name,
		Email:    body.Email,
		Password: password,
	}
	errTx := u.repoUsers.DoTransaction(ctx, func(ctxWithTx context.Context, dbt *gorm.DB) error {
		err := user.Create(dbt)
		if err != nil {
			errNew := errors.New("got error when create user")
			return errNew
		}
		return nil
	})

	if errTx != nil {
		return errTx
	}
	return nil

}

func (u usecase) UpdateUser(ctx context.Context, body request.UserRegis, user_id uint) error {
	// find user by email
	user, err := u.repoUsers.GetUserByID(ctx, user_id)
	if errors.Is(err, gorm.ErrRecordNotFound) || err != nil {
		fmt.Println(err)
		errNew := errors.New("user not found")
		return errNew
	}
	if body.Email != user.Email || !(body.Email == "") {
		user.Email = body.Email
	}
	if body.Name != user.Name || !(body.Name == "") {
		user.Name = body.Name
	}

	ok, _ := module.CheckPassword(body.Password, user.Password)
	if !ok || !(body.Password == "") {
		password, err := module.CreatePassword(body.Password)
		if err != nil {
			errNew := errors.New("got error when create user")
			return errNew
		}
		user.Password = password
	}

	errTx := u.repoUsers.DoTransaction(ctx, func(ctxWithTx context.Context, dbt *gorm.DB) error {
		err := user.Update(dbt)
		if err != nil {
			errNew := errors.New("got error when create user")
			return errNew
		}
		return nil
	})

	if errTx != nil {
		return errTx
	}
	return nil

}
