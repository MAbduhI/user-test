// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package uusers

import (
	"context"
	"errors"

	"gorm.io/gorm"
)

func (u usecase) DeleteUser(ctx context.Context, user_id uint) error {
	// find user by email
	user, err := u.repoUsers.GetUserByID(ctx, user_id)
	if errors.Is(err, gorm.ErrRecordNotFound) || err != nil {
		errNew := errors.New("user not found")
		return errNew
	}

	errTx := u.repoUsers.DoTransaction(ctx, func(ctxWithTx context.Context, dbt *gorm.DB) error {
		err := user.Delete(dbt)
		if err != nil {
			errNew := errors.New("got error when create user")
			return errNew
		}
		return nil
	})

	if errTx != nil {
		return errTx
	}
	return nil

}
