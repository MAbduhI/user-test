// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package uusers

import (
	"context"
	"errors"
	"gin-user-api/database/model"
	"gin-user-api/util/response"

	"gorm.io/gorm"
)

func (u usecase) GetAllUser(ctx context.Context) ([]response.UsersResp, error) {
	var all_users []model.Users
	var resp_users []response.UsersResp
	all_users, err := u.repoUsers.GetAllUser(ctx)
	if errors.Is(err, gorm.ErrRecordNotFound) {
		errNew := errors.New("users not available")
		return resp_users, errNew
	} else if err != nil {
		return resp_users, err
	}
	for _, d := range all_users {
		resp_users = append(resp_users, response.UsersResp{
			ID:    d.ID,
			Name:  d.Name,
			Email: d.Email,
		})
	}
	return resp_users, nil

}
func (u usecase) GetUserById(ctx context.Context, id uint) (*response.UsersResp, error) {
	user, err := u.repoUsers.GetUserByID(ctx, id)
	if errors.Is(err, gorm.ErrRecordNotFound) {
		errNew := errors.New("users not available")
		return nil, errNew
	} else if err != nil {
		return nil, err
	}

	return &response.UsersResp{
		ID:    user.ID,
		Name:  user.Name,
		Email: user.Email,
	}, nil

}
