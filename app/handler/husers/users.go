// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package husers

import (
	"fmt"
	"gin-user-api/app/usecase/uusers"
	"gin-user-api/util/middleware"
	"gin-user-api/util/request"
	"gin-user-api/util/response"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type handler struct {
	uUsers uusers.Usecase
}

func NewHandler(
	e *gin.Engine,
	uUsers uusers.Usecase,
) {
	handler := handler{
		uUsers: uUsers,
	}
	authorized := e.Group("/")
	authorized.Use(middleware.AuthMiddleware())
	{
		authorized.GET("/users", handler.Users)
		authorized.GET("/users/:id", handler.UsersId)
		authorized.PUT("/users/:id", handler.UpdateUsersId)
		authorized.DELETE("/users/:id", handler.DeleteUsersId)
		authorized.POST("/users", handler.UsersRegis)
	}

	e.POST("/login", handler.UsersLogin)
}

func (h handler) Users(c *gin.Context) {
	ctx := c.Request.Context()
	resp, err := h.uUsers.GetAllUser(ctx)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error find user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess(resp)
	c.JSON(code, data)
}

func (h handler) UsersId(c *gin.Context) {
	ctx := c.Request.Context()
	user_id := c.Param("id")
	user_id_int, err := strconv.Atoi(user_id)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error id user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	resp, err := h.uUsers.GetUserById(ctx, uint(user_id_int))
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error find user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess(resp)
	c.JSON(code, data)
}
func (h handler) UpdateUsersId(c *gin.Context) {
	ctx := c.Request.Context()
	var regisUser request.UserRegis
	user_id := c.Param("id")
	user_id_int, err := strconv.Atoi(user_id)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error id user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	err = c.BindJSON(&regisUser)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error read json", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	err = h.uUsers.UpdateUser(ctx, regisUser, uint(user_id_int))
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error find user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess("success update user")
	c.JSON(code, data)
}
func (h handler) DeleteUsersId(c *gin.Context) {
	ctx := c.Request.Context()
	user_id := c.Param("id")
	user_id_int, err := strconv.Atoi(user_id)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error id user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	err = h.uUsers.DeleteUser(ctx, uint(user_id_int))
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error find user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess("success delete user")
	c.JSON(code, data)
}
func (h handler) UsersRegis(c *gin.Context) {
	var regisUser request.UserRegis
	ctx := c.Request.Context()

	err := c.BindJSON(&regisUser)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error read json", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	err = h.uUsers.CreateUser(ctx, regisUser)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error create user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess("success create user")
	c.JSON(code, data)
}
func (h handler) UsersLogin(c *gin.Context) {
	var regisUser request.UserLogin
	ctx := c.Request.Context()

	err := c.BindJSON(&regisUser)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error read json", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	token, err := h.uUsers.Login(ctx, regisUser)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error login user", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess(gin.H{
		"token": token,
	})
	c.JSON(code, data)
}
