// Create by MAbduhI
// Gitlab https://gitlab.com/MAbduhI
// GitHub https://github.com/MAbduhI

package htasks

import (
	"fmt"
	"gin-user-api/app/usecase/utasks"
	"gin-user-api/util/request"
	"gin-user-api/util/response"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type handler struct {
	uTask utasks.Usecase
}

func NewHandler(
	e *gin.Engine,
	uTask utasks.Usecase,
) {
	handler := handler{
		uTask,
	}
	e.GET("/tasks", handler.GetAllTask)
	e.GET("/tasks/:id", handler.GetTask)
	e.POST("/tasks", handler.CreateTask)
	e.PUT("/tasks/:id", handler.UpdateTask)
	e.DELETE("/tasks/:id", handler.DeleteTask)
}
func (h handler) GetAllTask(c *gin.Context) {
	ctx := c.Request.Context()
	resp, err := h.uTask.GetAllTask(ctx)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error find tasks ", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess(resp)
	c.JSON(code, data)
}
func (h handler) GetTask(c *gin.Context) {
	ctx := c.Request.Context()
	task_id := c.Param("id")
	task_id_int, err := strconv.Atoi(task_id)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error id task", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	resp, err := h.uTask.GetTask(ctx, task_id_int)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error find tasks ", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess(resp)
	c.JSON(code, data)
}
func (h handler) CreateTask(c *gin.Context) {
	var createTasks request.TaskRequest
	ctx := c.Request.Context()

	err := c.BindJSON(&createTasks)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error read json", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	err = h.uTask.CreateTasks(ctx, createTasks)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error when create tasks", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess("success created task")
	c.JSON(code, data)
}
func (h handler) UpdateTask(c *gin.Context) {
	var updateTasks request.TaskRequest
	ctx := c.Request.Context()
	task_id := c.Param("id")
	task_id_int, err := strconv.Atoi(task_id)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error id task", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	err = c.BindJSON(&updateTasks)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error read json", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	err = h.uTask.UpdateTasks(ctx, updateTasks, task_id_int)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error when update tasks", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess("success updated task")
	c.JSON(code, data)
}

func (h handler) DeleteTask(c *gin.Context) {
	ctx := c.Request.Context()
	task_id := c.Param("id")
	task_id_int, err := strconv.Atoi(task_id)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error id task", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	err = h.uTask.DeleteTasks(ctx, task_id_int)
	if err != nil {
		code := http.StatusBadRequest
		data := response.ResponseTemp(code, "error when deleted tasks", fmt.Sprintf("got error : "+err.Error()))
		c.JSON(code, data)
		return
	}
	data, code := response.ResponseSuccess("success deleted task")
	c.JSON(code, data)
}
